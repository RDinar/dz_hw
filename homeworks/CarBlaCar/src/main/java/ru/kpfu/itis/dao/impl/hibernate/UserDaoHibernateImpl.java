package ru.kpfu.itis.dao.impl.hibernate;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.kpfu.itis.dao.UsersDao;
import ru.kpfu.itis.dao.factory.HibernateConnectionFactory;
import ru.kpfu.itis.model.User;

import java.util.List;

public class UserDaoHibernateImpl implements UsersDao {

    private static final Logger logger = Logger.getLogger(DriversDaoHibernateImpl.class);

    @Override
    public void save(User user) {
        Session session = null;
        try {
            session = HibernateConnectionFactory.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("error saving user by Hibernate", e.getCause());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public User findOne(Long userId) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findByNickname(String nickname) {
        Session session = null;
        User user = new User();

        try {
            session = HibernateConnectionFactory.getSessionFactory().openSession();
            /*user = (User) session.createCriteria(User.class)
                    .add(Restrictions.like("nickname", nickname)).list();*/
            Query query = session.createQuery("from User where nickname = :nickname");
            query.setParameter("nickname", nickname);
            session.beginTransaction();
            user = (User) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error("error find user by nickname Hibernate", e.getCause());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return user;
    }

    @Override
    public User findByEmail(String email) {
        return null;
    }

    @Override
    public User findByNicknameIgnoreCase(String nickname) {
        Session session = null;
        User user = new User();
        try {
            session = HibernateConnectionFactory.getSessionFactory().openSession();
            session.beginTransaction();
            user = (User) session.createCriteria(User.class)
                    .add(Restrictions.like("nickname", nickname).ignoreCase())
                    .list();
        } catch (Exception e) {
            logger.error("error find user by nickname ignore case Hibernate", e.getCause());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return user;
    }

    @Override
    public User findByEmailIgnoreCase(String email) {
        return null;
    }
}
