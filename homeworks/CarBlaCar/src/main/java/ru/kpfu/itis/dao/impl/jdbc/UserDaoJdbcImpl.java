package ru.kpfu.itis.dao.impl.jdbc;

import org.apache.log4j.Logger;
import ru.kpfu.itis.dao.UsersDao;
import ru.kpfu.itis.dao.factory.JDBCConnectionFactory;
import ru.kpfu.itis.dao.impl.hibernate.AutosDaoHibernateImpl;
import ru.kpfu.itis.model.User;

import java.sql.*;
import java.util.List;

public class UserDaoJdbcImpl implements UsersDao {

    private static final Logger logger = Logger.getLogger(AutosDaoHibernateImpl.class);
    Connection connection;
    PreparedStatement prst;
    ResultSet rs;

    private Connection getConnection() throws SQLException {
        return JDBCConnectionFactory.getInstance().getConnection();
    }

    @Override
    public void save(User user) {
        String query = "INSERT INTO users (id, avatar,  email, firstname, nickname, password, role, surname) VALUES (?,?,?,?,?,?,?,?)";
        try {
            connection = getConnection();
            prst = connection.prepareStatement(query);
            prst.setLong(1, user.getId());
            prst.setString(2, user.getAvatar());
            prst.setString(3, user.getEmail());
            prst.setString(4, user.getFirstname());
            prst.setString(5, user.getNickname());
            prst.setString(6, user.getPassword());
            prst.setString(7, user.getRole());
            prst.setString(8, user.getSurname());
            prst.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (prst != null) {
                    prst.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public User findOne(Long userId) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findByNickname(String nickname) {
        return null;
    }

    @Override
    public User findByEmail(String email) {
        return null;
    }

    @Override
    public User findByNicknameIgnoreCase(String s) {
        User user = null;
        String query = "SELECT * FROM users WHERE nickname = ?";
        String name = s.toLowerCase();
        try {
            connection = getConnection();
            prst = connection.prepareStatement(query);
            prst.setString(1, name);
            rs = prst.executeQuery();
            if (rs.next()) {
                user = new User();
                user.setId(rs.getLong(1));
                user.setAvatar(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setFirstname(rs.getString(4));
                user.setNickname(rs.getString(5));
                user.setPassword(rs.getString(6));
                user.setRole(rs.getString(7));
                user.setSurname(rs.getString(8));
            }
        } catch (SQLException e) {
            System.out.println("Exception findByNicknameIgnoreCase()" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (prst != null) {
                    prst.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    @Override
    public User findByEmailIgnoreCase(String email) {
        return null;
    }
}
