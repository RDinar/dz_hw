package ru.kpfu.itis.dao.impl.jdbc;

import ru.kpfu.itis.dao.DriversDao;
import ru.kpfu.itis.dao.factory.JDBCConnectionFactory;
import ru.kpfu.itis.model.Driver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DriversDaoJdbcImpl implements DriversDao {

    private JDBCConnectionFactory connectionFactory = JDBCConnectionFactory.getInstance();
    private ResultSet rs;
    private Statement statement;
    private Connection connection;

    @Override
    public void save(Driver driver) {
    }

    @Override
    public Driver findOne(Long id) {
        return null;
    }

    @Override
    public List<Driver> findAll() {
        return null;
    }

    @Override
    public List<Driver> findTop9ByOrderByRatingDesc() {
        String query = "SELECT * FROM drivers ORDER BY rating DESC LIMIT 9";
        List<Driver> drivers = null;
        try {
            connection = connectionFactory.getConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery(query);
            while (rs.next()) {
                Driver driver = new Driver();
                driver.setId(rs.getLong(1));
                driver.setRating(rs.getInt(2));
                drivers.add(driver);
                System.out.println(drivers);
            }
        } catch (SQLException e) {
            System.out.println("Exception findTop9ByOrderByRatingDesc(): " + e);
        } finally {
            try {
                if (rs!=null){
                    rs.close();
                }
                if (statement!=null){
                    statement.close();
                }
                if (connection!=null){
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return drivers;
    }
}
