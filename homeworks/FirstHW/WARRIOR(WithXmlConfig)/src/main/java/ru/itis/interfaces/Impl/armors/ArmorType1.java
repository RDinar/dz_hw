package ru.itis.interfaces.Impl.armors;

import ru.itis.interfaces.Armor;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */

public class ArmorType1 implements Armor {
    private String material;
    private int thickness;
    private int height;
    private int width;

    public ArmorType1() {
    }


    public void breakArmor() {
        System.out.println("Break(");
    }

    public void createArmor(String material, int thickness, int height, int width) {
        this.material = material;
        this.thickness = thickness;
        this.height = height;
        this.width = width;
        System.out.println("New Armor created" );
    }

}
