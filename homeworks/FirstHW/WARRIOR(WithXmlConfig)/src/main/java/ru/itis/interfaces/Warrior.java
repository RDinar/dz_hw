package ru.itis.interfaces;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */
public interface Warrior {
   // public int attack = 100;
    void attack(Warrior hater);
    void defend(Warrior hater);
    Warrior getHater();
}
