package ru.itis.interfaces.Impl.warriors;

import ru.itis.interfaces.Armor;
import ru.itis.interfaces.Warrior;
import ru.itis.interfaces.Weapon;


/**
 * Created by Dr.Raim on 16-Feb-17.
 */
public class CompWarrior implements Warrior {
    private String name;
    private int level;
    private int health;
    private Weapon weapon;
    private Armor armor;
    public CompWarrior() {
    }

    public CompWarrior(Weapon weapon, Armor armor) {
        this.weapon = weapon;
        this.armor = armor;
    }

    public CompWarrior(String name, int level, int health) {
        this.name = name;
        this.level = level;
        this.health = health;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }


    public void attack(Warrior hater) {
        System.out.println( this.getName() +" attacked ");
    }

    public void defend(Warrior hater) {
        System.out.println( this.getName() + " Defend from " + hater.getClass().getDeclaredClasses().getClass());
    }

    public Warrior getHater() {
        return null;
    }
}
