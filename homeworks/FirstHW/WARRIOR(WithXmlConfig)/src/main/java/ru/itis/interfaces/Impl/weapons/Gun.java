package ru.itis.interfaces.Impl.weapons;
import ru.itis.interfaces.Weapon;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */

public class Gun implements Weapon {
    private static final int COUNT_CHARGE = 100;
    public void charge() {
        System.out.println("Recharged");
    }
}
