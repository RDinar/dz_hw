package ru.itis.battlefield;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.interfaces.Impl.warriors.CompWarrior;
import ru.itis.interfaces.Impl.warriors.TatarWarrior;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */
public class Battle {
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-bean-config.xml");

        TatarWarrior tatarWarrior = (TatarWarrior) context.getBean("tatarWarrior");
        CompWarrior compWarrior = (CompWarrior) context.getBean("compWarrior");
        tatarWarrior.attack(compWarrior);
        tatarWarrior.defend(compWarrior);

    }
}
