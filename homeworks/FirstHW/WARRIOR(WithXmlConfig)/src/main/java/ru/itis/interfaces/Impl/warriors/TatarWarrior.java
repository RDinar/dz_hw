package ru.itis.interfaces.Impl.warriors;

import ru.itis.interfaces.Armor;
import ru.itis.interfaces.Warrior;
import ru.itis.interfaces.Weapon;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */

public class TatarWarrior implements Warrior {
    private String name;
    private int level;
    private int health;
    private int attack;
    private int defence;

    private Weapon weapon;
    private Armor armor;

    public TatarWarrior() {
    }

    public TatarWarrior(Weapon weapon, Armor armor) {
        this.weapon = weapon;
        this.armor = armor;
    }

    public TatarWarrior(String name, int level, int health, int attack, int defence) {
        this.name = name;
        this.level = level;
        this.health = health;
        this.attack = attack;
        this.defence = defence;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }


    public void attack(Warrior hater) {
        System.out.println(this.getName() + " is attacking " + hater.getClass().getSimpleName());
    }

    public void defend(Warrior hater) {
        System.out.println(this.getName() + " is defending from " + hater.getClass().getSimpleName());

    }

    public Warrior getHater() {
       // return Warrior.class.getDeclaredClasses().getClass();
        return null;
    }

}
