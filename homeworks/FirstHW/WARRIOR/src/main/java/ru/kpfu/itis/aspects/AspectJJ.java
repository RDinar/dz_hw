package ru.kpfu.itis.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
*Created by Dr.Raim on 07-Mar-17.
 **/

@Component
@Aspect
public class AspectJJ {
    @Before("execution(* ru.kpfu.itis.interfaces.Impl.warriors.CompWarrior.attack())")
    private void get() {
        System.out.println("hbk");
    }
}
