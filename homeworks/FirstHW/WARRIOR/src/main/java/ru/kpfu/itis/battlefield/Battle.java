package ru.kpfu.itis.battlefield;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kpfu.itis.AppConfig;
import ru.kpfu.itis.interfaces.Impl.armors.ArmorType1;
import ru.kpfu.itis.interfaces.Impl.warriors.CompWarrior;
import ru.kpfu.itis.interfaces.Impl.warriors.TatarWarrior;
import ru.kpfu.itis.interfaces.Impl.weapons.Gun;


/**
 * Created by Dr.Raim on 14-Feb-17.
 */
public class Battle {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        TatarWarrior tatarWarrior = (TatarWarrior) context.getBean("tatarWarrior");
        CompWarrior compWarrior = (CompWarrior) context.getBean("compWarrior");

        ArmorType1 armorType1 = (ArmorType1) context.getBean("armorType1");
        Gun gun = (Gun) context.getBean("gun");

        tatarWarrior.setOpponent(compWarrior);
        tatarWarrior.setArmor(armorType1);
        tatarWarrior.setWeapon(gun);
        tatarWarrior.setName("John Cena");

        compWarrior.setName("Super Computer HN7546");
        tatarWarrior.attack();
        tatarWarrior.defend();
        compWarrior.attack();

    }
}
