package ru.kpfu.itis.interfaces;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */
public interface Armor {
    void breakArmor();
    void createArmor(String material, int thickness, int height, int width);
}
