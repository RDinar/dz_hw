package ru.kpfu.itis.interfaces.Impl.warriors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.interfaces.Weapon;
import ru.kpfu.itis.interfaces.Armor;
import ru.kpfu.itis.interfaces.Warrior;


/**
 * Created by Dr.Raim on 14-Feb-17.
 */
@Component
public class TatarWarrior implements Warrior {
    private String name;
    private int level;
    private int health;
    private int attack;
    private int defence;
    private Warrior opponent;

    private Weapon weapon;
    private Armor armor;

    public TatarWarrior() {
    }

    @Autowired
    public TatarWarrior(@Qualifier(value = "gun") Weapon weapon, @Qualifier(value = "armorType1") Armor armor) {
        this.weapon = weapon;
        this.armor = armor;
    }

    public TatarWarrior(String name, int level, int health, int attack, int defence) {
        this.name = name;
        this.level = level;
        this.health = health;
        this.attack = attack;
        this.defence = defence;
    }

    public Warrior getOpponent() {
        return opponent;
    }

    public void setOpponent(Warrior opponent) {
        this.opponent = opponent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Armor getArmor() {
        return armor;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }


    public void attack() {
        System.out.println(this.getName() + " attacking " + getOpponent().getClass().getSimpleName());
    }

    public void defend() {
        System.out.println(this.getName() + " defending from " + getOpponent().getClass().getSimpleName());

    }


}
