package ru.kpfu.itis.interfaces.Impl.armors;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.interfaces.Armor;

/**
 * Created by Dr.Raim on 16-Feb-17.
 */
@Component
public class ArmorType2 implements Armor {
    public void breakArmor() {
    }

    public void createArmor(String material, int thickness, int height, int width) {
    }
}
