package ru.kpfu.itis.interfaces.Impl.weapons;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.interfaces.Weapon;

/**
 * Created by Dr.Raim on 14-Feb-17.
 */
@Component
public class Gun implements Weapon {
    public void charge() {
        System.out.println("Recharged");
    }
}
