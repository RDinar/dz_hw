package ru.kpfu.itis;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


/**
 * Created by Dr.Raim on 17-Feb-17.
 */
@Configuration
@ComponentScan(basePackages = "ru.kpfu.itis")
@EnableAspectJAutoProxy

public class AppConfig {

}
