package ru.kpfu.itis.interfaces;


/**
 * Created by Dr.Raim on 14-Feb-17.
 */
public interface Warrior {
    void attack();
    void defend();
}
